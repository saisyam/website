+++
title = "Automation testing of webcam applications"
description = "We can automate web application using Selenium or splinter. What about web applications that use webcam? You know, with webdriver you can create a fake webcam device. In this article I will show you how to create media for testing web applications that use webcam."
author = "Saisyam"
date = "2019-12-13"
categories = ["testing"]
tags = [
    "web testing",
    "splinter",
    "ffmpeg",
]
+++

{{< adsense type="horizontal" >}}

The challenge here is to fake the webcam with proper video format. In this post I am using Chrome browser along with [Splinter](https://splinter.readthedocs.io/en/latest/). 

If you are new to Splinter, check out the link provided and I promise you love it. We use Splinter not only for testing but also for web scraping. Chrome need Y4M format to replace the camera feed. To generate that format you need [ffmpeg](https://www.ffmpeg.org/) tool. Using that tool you can convert any MP4 video into Y4M format.

> Y4M is an un-compressed format. The converted Y4M file size will be almost three times the MP4 file. So, take a small MP4 file or repeat the image to create the video.

Once you have the Y4M file you can follow the below mentioned steps to fake the webcam. [Here](https://testrtc.com/y4m-video-chrome/) is an article on how to create Y4M files using ffmpeg.

## Chrome Settings

We will set `webdriver.ChromeOptions` to enable media stream using a fake device.

```python
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--use-fake-device-for-media-stream")
chrome_options.add_argument("--use-file-for-fake-video-capture=./salma_hayek.y4m")
chrome_options.add_experimental_option("prefs", {
    "profile.default_content_setting_values.media_stream_camera": 1
})
```

{{< adsense type="article" >}}

You are all set to test the web applications uing a fake video. The complete source code along with the sample web application (using Python Flask) is available as a [gitlab project](https://gitlab.com/saisyam/web-automation-testing/-/tree/master/fake_webcam).

