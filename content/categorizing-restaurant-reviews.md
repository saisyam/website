+++
title = "Categorizing Restaurant Reviews"
description = "Customer reviews are the most valuable asset for a restaurant. These reviews help restaurants to improvise their services to make customers happy. But, analyzing each and every review manually to understand the intent of the customer is difficult and time consuming process. Python provides lot of tools to process reviews and categorize them."
author = "Saisyam"
date = "2020-01-20"
categories = ["machine learning"]
tags = [
    "text categorization",
    "Spacy",
    "Python"
]
+++

{{< adsense type="horizontal" >}}

To create a machine learning model to automatically categorize a review, we need to have lot of valid labelled data to train our model. In this post we will try to extract information from different reviews which will help us in identifying the category and label the review as appropriate.

Following are the list of items (let's stick to some important ones) that plays a major role in Food or a restaurant business:

1. Quality of the Food
2. Service at the restaurant
3. Ambience of the restaurant.
4. Price or cost of the food

Users will rate the restaurant and write a review keeping the above items in mind. Let's look at a sample review:

> Great cocktails and very good food.  Overall first class restaurant. - 4.0

The above review is a positive review. User has given 4.0 star rating and mentioned about the quality of the food. We can label this review under "food".

Let's look at one more review:

> Awful place. Staff is not nice, very rude.  They never seem to want to help at all..Their service is not up to par & always out of bagels - 1.0

This is a negative review. User is not happy with the staff or the service they provide. He has given 1.0 star for this review. There are two things user mentioned in this review:

1. `Awful place` is related to ambience
2. `Rude staff`, `service is not up to par` related to service

We can categorize or label this review under "service" and "ambience".

By reading the reviews we can identify what user is talking about, for example, bad food, good service etc. We need a dataset with labelled data to train our model. To create dataset manually is a long process. Let's see whether we can automatically create the dataset with proper labelling with the information in hand, the rating and the review text.

## Technology
{{< adsense type="article" >}}
We use [Python Spacy](https://spacy.io/) to analyze text. Spacy has lot many features but I am using simple features, like tokenization, noun chunks etc, to identify the category of the text.

For this activity I used [Yelp dataset](https://www.yelp.com/dataset/challenge). Yelp dataset contains reviews from various businesses. I filtered the reviews for Food and restaurant industry which can be done using simple Python JSON parser. I have taken 100k reviews related to food industry.

To setup spacy refer to this [link](https://spacy.io/usage).

We need to remove [stopwords](https://kavita-ganesan.com/what-are-stop-words) from our text. We use the following code snippet to remove stopwords.

<script src="https://gitlab.com/saisyam/text-analytics/snippets/1941976.js"></script>

I have used a set of following keywords to filter the noun chunks extracted using spacy to identify the category.

<script src="https://gitlab.com/saisyam/text-analytics/snippets/1941977.js"></script>

I have grouped few keywords into a single category, for example, `service` and `staff` merged to `service`. The rest of the code is reading the reviews JSON file and creating the file with categories. Complete source code is available at [Gitlab](https://gitlab.com/saisyam/text-analytics/-/tree/master/food_reviews). In the next article we will build a model using this data.
{{< adsense type="article" >}}