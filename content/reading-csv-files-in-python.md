+++
title = "Reading CSV File in Python"
description = "When it comes to data processing, Python comes first. The complex task in data processing is creating structured data called data lake from various sources like files, databases, logs, audio, video etc. Python comes with common file processing packages as part of its distribution. In this article we will use `csv` and `pandas` packages to read CSV files."
author = "Saisyam"
date = "2020-04-04"
categories = ["data analytics"]
tags = [
    "csv",
    "python",
    "pandas"
]
+++

{{< adsense type="horizontal" >}}

Today data is available in various formats, like, CSV, JSON, TXT etc. Python provides all the necessary tools to parse these files. In this article we will learn how to parse a large CSV file using Python.

We will use Python `csv` package and `pandas` to parse the CSV file. I have taken a CSV file of baby names from [here](https://raw.githubusercontent.com/hadley/data-baby-names/master/baby-names.csv). This file contains 250K records of baby names. I am using this data to generate user database to test the login API I have built.

## Reading with Python `csv` package
The goal is read the name from the existing CSV file and create an `email` which is `name` + `4 digit random number` + ` @gmail.com` and `password`, a ten digit random string. I will be using Python `string` and `random` packages to achieve this. Let's look at random string function.

```python
def random_string(length=10):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))
```

The above method generates a random string of default 10 characters. Following code will iterate over the record in CSV and create above mentioned fields using the `random_string` function.

```python
def iter_records(file_name):
    with open(file_name, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            record = []
            record.append(row['name'])
            fake_email = row['name'].lower()+
                         str(random.randint(1000, 9999))+"@gmail.com"
            record.append(fake_email)  
            password = random_string()
            record.append(password)
            yield record
```

Finally we will use the above two functions to generate the user data we need.

{{< adsense type="article" >}}

```python
csv_file_path = "../files/baby-names.csv"
out_file_path = "../files/user-data.csv"

with open(out_file_path, 'w', newline='') as out_file:
    writer = csv.writer(out_file)
    writer.writerow(['name','email','password'])
    for i, record in enumerate(iter_records(csv_file_path)):
        writer.writerow(record)
    print("Total records: ", i)
out_file.close()
```
The above code will read the input CSV file and write back the `name`, `email` and `password` to the output CSV file. The complete Jupyter Notebook is available at [Github](https://github.com/saisyam/python-tools/blob/master/readcsv.ipynb).


## Reading with `pandas` package

`pandas` provides methods to read and manipulate CSV files in few lines of code. The following snippet will show how to load a CSV file and display the top five records:

```python
csv_file = "./files/baby-names.csv"
df = pd.read_csv(csv_file)
df.head()
```
The input CSV file contains four columns, `year`, `name`, `percent` and `sex`. We need only the `name` field, so we drop other fields.

```python
df.drop(['year','percent','sex'], axis=1, inplace=True)
df.head()
```
The `drop` method will drop the fields from the dataframe, `axis=1` specifies we want to remove the column and `inplace=True` will change the current data frame instead of creating a new one with the deleted columns. `head` will display the top 5 records in the dataframe.

Now we add two new fields to dataframe, `email` and `password` created in the same way as mentioned above:

```python
out_csv = "./files/user_data_pd.csv"
email = []
password = []
for name in df['name']:
    email.append(name.lower()+str(random.randint(1000, 9999))+"@gmail.com")
    password.append(random_string())
df['email'] = email
df['password'] = password
df.to_csv(out_csv)
df.head()
```

The complete Jupyter Notebook is available at [Github](https://github.com/saisyam/python-tools/blob/master/readcsv_pd.ipynb)
{{< adsense type="article" >}}