+++
title = "Understanding Microservices"
description = "Microservices is a group of small services which will perform a specific task. All the services will communicate among themselves to form a bigger system. In this article we will discuss the advantages and complexities invovled in adopting Microservices architecture."
author = "Saisyam"
date = "2020-02-22"
categories = ["cloud"]
tags = [
    'microservices',
    'rabbitmq',
    'redis',
]
+++
{{< adsense type="horizontal" >}}

Microservices are applications that are built as loosely coupled services that interact with each other through popular communication protocols such as, HTTP, TCP or Message Queues. Each microservice is responsible for a single feature.

Let's take a real world example, an online shopping portal. There are lot of components in an online shopping portal. To name a few,


*  Users
*  Products
*  Orders
*  Payment and Shipping
*  Analytics and Reporting

Each of the above components can be built as a single or multiple microservice(s). Communication between microservices is handled through standard protocols like HTTP/TCP etc. Most of the services uses messaging queues (RabbitMQ or Redis) as a communication channel.

## Advantages of Microservices

*  **Focussed and small code base** - Each microservice can be managed by a single small team focussed on the core business logic. Minimum dependencies when compared with a whole application and touching the small portion of code instead of breaking the complete system.
*  **Agile** - Each service can be deployed independently and it will be easier to manage bug fixes and releases. Services can be updated without redeploying the entire application. 
*  **Mix of Technologies** - Services don't need to use or share the same technology stack or frameworks. Teams can chose their own technology stack which seems appropriate for their work.
*  **Scalability** - Services can be scaled independently based on the load and resources required without scaling the entire application. In the above example, on a sale day, orders service may need more resources and can scaled independently based on the load.
*  **Fault isolation** - If any service goes down or unavailable, it won't bring down the complete application as long as the dependent services are designed to handle faults (this is called circuit breaking).

## Things to keep in mind

{{< adsense type="article" >}}

Though there are advantages of microservices architecture, we need to keep certain things in mind when the application contains so many microservices:

* **Complexity** - With many small microservices the application as a whole is more complex. For example, Netflix uses more than 500 microservices to stream content.
*  **Development and Testing** - A service may dependent on other services which are already built are still in development. You need to have a clean design with service boundaries defined properly in order to build and test the service.
*  **Too many technologies and frameworks** - All services may not use the same technology stack or framework. You will end up with so many different programming languages and frameworks which will be difficult to manage going forward.
*  **Interservice communication** - There will be more interservice communication happens in microservice architecture. You need to design your communication process carefully to avoid latency. Use of asynchronous communication patterns (Messaging queues) may be helpful.
*  **Updating, Mananging and Monitoring** - Lot of automation is required in updating, managing and monitoring microservices. You need to have a strong DevOps culture to manage this. I think today most of the organizations made DevOps as part of their development itself.
*  **Data Integrity** - Each service is responsible for its own data persistence. As a result data consistency can be a challenge.

## Why Microservices are so popular?

Today, World is moving towards containerization and cloud computing. We have many tools to containerize/manage/monitor applications on the cloud. Many companies are migrating to cloud instead of maintaining their own infrastructure. This migration process made them to adapt Continuous Integration/Deployment and DevOps into their culture. Development process became Agile.The availability of containers and their orchestration tools has also contributed to increasing microservices adoption. 

## Conlcusion

In the coming articles we will build microservices using [Python Nameko](https://www.nameko.io/), deploying and scaling microservices with docker containers and kubernetes. We will also look into different APM (Application Performance Monitoring) tools like Grafana, Prometheus etc.  
{{< adsense type="article" >}}